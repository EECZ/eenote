{
    "id": "ed169aad-f775-48c2-8e9c-a04d912a002e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6916698a-3a86-4696-ac1c-3a1c5d3b933b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed169aad-f775-48c2-8e9c-a04d912a002e",
            "compositeImage": {
                "id": "7f864572-874f-4453-a54f-ef11a5391fe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6916698a-3a86-4696-ac1c-3a1c5d3b933b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "323d85a6-e9a2-4a18-8a45-f309ca84a04a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6916698a-3a86-4696-ac1c-3a1c5d3b933b",
                    "LayerId": "81d55883-f435-4f95-ab58-75c18504988d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "81d55883-f435-4f95-ab58-75c18504988d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed169aad-f775-48c2-8e9c-a04d912a002e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}