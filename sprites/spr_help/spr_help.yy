{
    "id": "f2c0a92b-4972-4fc4-aaf2-2498cc798612",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_help",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "061fe76c-3863-4589-a3a6-c21fe60be1b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2c0a92b-4972-4fc4-aaf2-2498cc798612",
            "compositeImage": {
                "id": "c68e86a0-9ece-4acb-899d-467dd5288cd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "061fe76c-3863-4589-a3a6-c21fe60be1b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaab1304-6384-41b9-b980-0897b0ef882a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "061fe76c-3863-4589-a3a6-c21fe60be1b9",
                    "LayerId": "d11f1b0d-c7b4-40bf-9dfe-5deee99f0ce7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d11f1b0d-c7b4-40bf-9dfe-5deee99f0ce7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2c0a92b-4972-4fc4-aaf2-2498cc798612",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}