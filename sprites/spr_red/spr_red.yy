{
    "id": "615c01e2-6131-424f-97bd-0ca05d575fb1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2da58d00-9f21-4315-98a2-391ff41abbad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615c01e2-6131-424f-97bd-0ca05d575fb1",
            "compositeImage": {
                "id": "2b33d5a0-eab2-4dfe-8e18-758d3a8901dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2da58d00-9f21-4315-98a2-391ff41abbad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9963cb84-48a0-42d1-ab9f-72db0da8497d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2da58d00-9f21-4315-98a2-391ff41abbad",
                    "LayerId": "d3bc7772-be09-41d5-9fcc-3200fd3f7960"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d3bc7772-be09-41d5-9fcc-3200fd3f7960",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "615c01e2-6131-424f-97bd-0ca05d575fb1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}