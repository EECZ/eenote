{
    "id": "14f1b54b-da8c-40db-9e43-9f10efa4aa97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "96535da1-1d63-4b54-b9f5-9e73cc9dc38b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14f1b54b-da8c-40db-9e43-9f10efa4aa97",
            "compositeImage": {
                "id": "dc6ab5fb-7ddc-4178-bee5-451b7ae47dc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96535da1-1d63-4b54-b9f5-9e73cc9dc38b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18cd1640-f0bc-44af-80e5-cfce7492f4c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96535da1-1d63-4b54-b9f5-9e73cc9dc38b",
                    "LayerId": "4aeccc9d-ce9b-47fa-bb2b-138d64f0c5b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4aeccc9d-ce9b-47fa-bb2b-138d64f0c5b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14f1b54b-da8c-40db-9e43-9f10efa4aa97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}