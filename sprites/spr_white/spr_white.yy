{
    "id": "c84a0133-f7b5-4879-9e40-aa74e2386053",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b12a6c0b-41c9-45ea-b88f-9af926eaa808",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c84a0133-f7b5-4879-9e40-aa74e2386053",
            "compositeImage": {
                "id": "579fe00f-1f69-412c-a902-2cbbff58584e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b12a6c0b-41c9-45ea-b88f-9af926eaa808",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b6b8c5c-3603-4d61-82f6-26051356820a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b12a6c0b-41c9-45ea-b88f-9af926eaa808",
                    "LayerId": "c02eeee5-40d8-45da-877d-462316713b95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c02eeee5-40d8-45da-877d-462316713b95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c84a0133-f7b5-4879-9e40-aa74e2386053",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}