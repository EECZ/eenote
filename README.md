# Read me#

##About this project##
####EENote is free and open-source project written in GML(Game Maker Language) using GameMaker: Studio 2####

##How can I help ?##
####It's simple, follow these steps:####
####1) Download project file [here](https://bitbucket.org/EECZ/eenote/get/8b7f447fb408.zip).#####
####2) Improve it.####
####3) Send me your project file on this [email](mailto:help.entent@gmail.com).####
####4) Also you can simply link it and commit it!####
####5) And don't forgot to share it!####
####6) Or send me a feedback on the same email.