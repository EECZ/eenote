var txt = global.text;
if show_question("Do you want to create EENote file (*.een)?")
{
	if(file_exists("my_text.een")) file_delete("my_text.een");
	ini_open("my_text.een");
	ini_write_string("EENote","Saved_Text",txt);
	show_message_async("The file was saved as EENote file. You will found it at: %appdata%\EENote");
    ini_close();

}
else{
	if(file_exists("my_text.txt")) file_delete("my_text.txt");
	ini_open("my_text.txt");
	ini_write_string("EENote","Saved_Text",txt);
	show_message_async("The file was saved as *.txt file. You will found it at: %appdata%\EENote");
    ini_close();
}