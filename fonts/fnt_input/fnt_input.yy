{
    "id": "98e00119-9da8-47b9-babd-6d75b065872f",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_input",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "09a588fb-0570-4af4-92af-88024be537be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c2abe8a6-31ec-4f9b-a86b-51616dd76ac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 14,
                "offset": 2,
                "shift": 5,
                "w": 1,
                "x": 211,
                "y": 122
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f71be4fc-ca16-4db7-a318-427919a38088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 6,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 173,
                "y": 122
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "759e316c-e306-4790-b41d-058ef52817ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 227,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f23062ac-1fbb-4cfe-b810-7c3978916c1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 22,
                "y": 82
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "08ea0b64-e2e1-4e9a-a672-bdcf15d78ae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 14,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c9b2bb1a-462d-4a76-adea-618295abd6f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 175,
                "y": 22
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b8bd5ff6-e8dc-42e2-afd2-405af45ddb89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 6,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 245,
                "y": 122
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "dd4c7de4-4e7b-4ffa-8bc8-aa9227aae1c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 84,
                "y": 122
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "72bb5aa1-263e-484a-bfe0-04789748b85b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 89,
                "y": 122
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c7a74f42-a664-426d-ab28-36077b5efb3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 144,
                "y": 122
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "82ac54a5-6424-4e8f-8049-b5c595d0463c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 12,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 113,
                "y": 102
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "92b27503-d7ab-4152-990c-9d45bc155274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 2,
                "shift": 4,
                "w": 1,
                "x": 208,
                "y": 122
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a088bc6f-7458-42e0-9a5b-fa08663fda07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 94,
                "y": 122
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2fee6f42-1f7f-4f77-b34d-a2b17cce385d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 14,
                "offset": 2,
                "shift": 4,
                "w": 1,
                "x": 226,
                "y": 122
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9ec7a294-6167-437e-9eec-f79b02b65e5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 49,
                "y": 122
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ad6381e2-8386-49bd-b9db-8ad645f543d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 237,
                "y": 82
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "bc121378-81d8-42e8-91b0-4f5b79ab27bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 14,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 43,
                "y": 122
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d3ef637b-54fa-4205-8733-bfff5dad7f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 239,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a7efb36b-59d2-4d14-95f0-bd2a8bd2e995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 219,
                "y": 82
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7f4b9660-e4f9-4bb3-ada3-97e2a33dd233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fab57fdc-b3a4-4090-afcf-7f3d77233846",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 192,
                "y": 82
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d1a8b928-b4f6-48a9-b6ed-04f4313e503d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 183,
                "y": 82
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "200c463c-3745-4833-93ee-77d3b2509297",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 14,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 196,
                "y": 102
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "fca854fe-dccc-428b-ac26-39118d12a28b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 165,
                "y": 82
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b4486fae-825a-4a3c-9563-366556253f45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 129,
                "y": 82
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8a64c6f0-7789-4014-9738-2006b38e0aae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 14,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 217,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "58289135-e4cc-4d95-8458-217a42fc520c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 205,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6b2c48cc-e167-4e3b-aa28-76c1a38efa1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 12,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 178,
                "y": 102
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e3839814-038a-4b0e-b4bb-fc2c95e4bfbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 11,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 237,
                "y": 102
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3e577760-45bb-44ad-b301-430ad917d917",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 12,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 187,
                "y": 102
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8fe58cd1-cb30-40d1-b713-206c19e0776c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 120,
                "y": 82
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "55d25805-22c1-486d-bed1-677055e59f4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "1ae6b38b-2ee7-4066-acaa-dc1f7e606047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b2d7611f-e9a0-4668-b626-951348256fc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 112,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "bbba48ae-813d-4de3-aaab-51e0bd074d5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 211,
                "y": 22
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "94b8b089-eab5-4ba6-8f2c-cc414b2d8291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 223,
                "y": 22
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c9b1475d-cb90-4cdd-a84b-f7e82a8dd27d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 79,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9f77123f-0c7e-4aca-9f30-9cfb7094944a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 199,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "665aea62-8d6a-49c2-9ade-6f35d6990249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 151,
                "y": 22
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "19f1a32d-692f-48ac-b253-8ad868fe0ea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 90,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0d3c31fd-6e1d-49de-a7bd-60eb7b781c6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 220,
                "y": 122
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ada55894-2b3c-422c-8084-09729438f834",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 48,
                "y": 82
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ce97e01b-7d54-42c0-9211-755891a0eaed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 194,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "42461f00-bd93-45aa-8f8b-fc6d982ded0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 102
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a6a2d559-5235-4c45-8028-1dbeef02fe9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 14,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "198301f7-b279-4a07-8cca-8de3552b255f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 101,
                "y": 62
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f58c1a02-0192-49c0-98e5-eaa8a61de136",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2f76f863-8285-4323-a96d-9bf72bb4b544",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 68,
                "y": 62
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "1e93e81d-af59-4c1f-87ab-1d71795ec81c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 127,
                "y": 22
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "77b5d2e8-e701-4fa3-ab0f-ce759a8d924b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 187,
                "y": 22
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e01d3a3c-0b7b-4c4d-b6b0-199feaa031e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 57,
                "y": 62
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4703fe2e-d9d7-43e6-86f0-b4b8bd09573f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 205,
                "y": 42
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "152d5e83-90f3-4f6c-b6c3-243d063b81d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 62
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0eacc9cb-90f1-495e-94f0-3e9590d58a7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 28,
                "y": 22
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f02d2832-0623-4e8a-8bef-40654f5f7095",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 14,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1ac93a10-885d-4729-8d8c-e43ec5591516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "405aef93-6181-4377-a5f4-945b715d2262",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 150,
                "y": 42
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c1144710-ce77-4c58-b59b-97952b93c5ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 128,
                "y": 42
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f823c05e-55c8-4d8d-9e6c-55f076607c9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 74,
                "y": 122
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "91d68e83-4a43-4bd0-b4bf-0797131e8992",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 25,
                "y": 122
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "69ed083a-a93c-4931-bd9f-1581a298bcc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 69,
                "y": 122
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a5c88df5-eae5-450b-9b1a-ceb1fa8186ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 8,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 55,
                "y": 122
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "861295f0-a2b7-46fc-bec4-57f7d9eaf993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 93,
                "y": 22
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e2a8544f-d493-43b8-8a27-86f520fffb00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 4,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 241,
                "y": 122
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d254c977-6a19-4860-be06-37084efb7fa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 93,
                "y": 82
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "420d9388-8011-4f21-a455-2dd3cd13d797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 102,
                "y": 82
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "211b1f2d-1ba9-46d5-b363-07f0e590d39b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 162,
                "y": 102
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "84f93f95-decd-44fe-be64-a87ba24e2173",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 138,
                "y": 82
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "afdde710-f8da-4e28-98e5-65d7533d7f8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 147,
                "y": 82
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4e3cd2bf-4472-4003-813b-cd5f536ecca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 18,
                "y": 122
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1dda60d4-b0cb-4542-bfb0-b3197661169f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 66,
                "y": 42
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "63a080e6-8d7c-4967-b5cf-7b1ef149f465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 154,
                "y": 102
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "110c207b-a0f4-4529-975a-53d9db5d9837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 14,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 214,
                "y": 122
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d73b4d92-448a-4e53-a90c-86867604afb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 79,
                "y": 122
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7404f093-41f0-4f2c-8296-4224715d3433",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 189,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "3d69dc9d-dd92-4cac-8491-730a016b1fca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 223,
                "y": 122
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e4e73496-8c52-4366-8db6-9d0a1480a651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 14,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 67,
                "y": 22
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ab5ea7d6-5a3c-4dab-b820-337a7219d975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 130,
                "y": 102
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b3afc768-4f50-43c2-b3d5-7562498bd9d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 169,
                "y": 62
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7ac94432-6230-42f3-b2c8-b5d9b804f4b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 141,
                "y": 62
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ff664081-ec83-45cd-a215-f6a85639b251",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 132,
                "y": 62
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d6b52e14-19c4-4f27-b792-54fb86fcbb7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 14,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 31,
                "y": 122
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "69de39ab-fecf-4704-af18-ce2a4debca8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 122,
                "y": 102
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f65ec5c1-9223-4bac-a7b4-61f98aac5615",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 37,
                "y": 122
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "581bbae2-3127-4679-9d7e-97665b34660b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 146,
                "y": 102
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5ed183e3-6431-4698-817c-aef67a200186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 14,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 46,
                "y": 62
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "79e9462a-a07f-4ad1-95da-db9a4a85453c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5c564dcf-3a61-4552-b47d-f5864f0c3c76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 39,
                "y": 82
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c01c32cd-6be1-4d86-857b-5d3abece5d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 26,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "84442cd8-ef66-4b59-935b-07dd5c00c430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 102
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b6dc4114-f4b8-4574-b3ac-3f2a539a8356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 67,
                "y": 102
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "eb92cffc-33d6-47f8-bf08-b29483689987",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 196,
                "y": 122
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6e9de17f-632a-4f11-8f49-bc090ded7e9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 74,
                "y": 102
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "684abc82-2b10-4eda-a36f-cefb0d54dc11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 8,
                "y": 122
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "425a1b32-a69e-4b81-9571-1ee1cd8e7829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 0,
                "x": 248,
                "y": 122
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "43fd91d7-ae8e-473e-b0cf-3cc54f695571",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 17,
                "offset": 2,
                "shift": 5,
                "w": 1,
                "x": 199,
                "y": 122
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "710cd21e-0285-4ff1-a8d7-5b6d6a37d261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 97,
                "y": 42
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "44b49924-9463-4313-bdaa-a73085c724c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 179,
                "y": 62
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "ed54719e-9f51-487d-ac5c-bbacde7314dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 11,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 57,
                "y": 102
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "292f7f95-8543-4361-8bf3-3e6103f2959c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 106,
                "y": 42
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "9b2dfd8d-5bf5-4f36-9c0e-e18dffcfd788",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 17,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 202,
                "y": 122
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "a063e1ac-9044-436b-884e-9bfa5ca884e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 56,
                "y": 42
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "2d5fd246-4069-41fe-ac06-be1201986275",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 3,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 232,
                "y": 122
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "2935fe6c-fa1e-4011-a40f-ece80fe99181",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 14,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "64115c7f-028a-4334-a9cd-8e5d65f3826e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 137,
                "y": 122
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "a5524d10-016f-41f1-b575-43661bdd2939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 220,
                "y": 102
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "e5a8e79a-ab7d-4ed2-8621-5c1d2497e339",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 11,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 246,
                "y": 102
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "62b9ee2f-14c9-4184-8232-0d641af383e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 100,
                "y": 122
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "449f3e00-dbe4-4d71-8d79-4ee9fd8414d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 14,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "6826d796-cb35-462f-b990-cff4efaabc9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 2,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 184,
                "y": 122
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "146cdf34-a1b2-4764-a329-40b7d291a9db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 6,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 167,
                "y": 122
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "0f1fd8c6-bd7e-472e-892a-576876b260d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 66,
                "y": 82
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "035687d1-cc81-4a58-974c-e01c620e30ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 123,
                "y": 122
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "fa836b39-5d52-4f4f-8bd9-e054a6fa5aae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 130,
                "y": 122
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "08f0ae81-5283-4b1c-8340-13ab280fb74a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 4,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 237,
                "y": 122
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "da30f704-209d-4070-a949-91f95db84c6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 150,
                "y": 62
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "89b67d14-a02d-41d2-822e-a6db0a14747d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 104,
                "y": 22
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "27a1df0f-c7d9-4bbb-8693-62ff8b56f355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 9,
                "offset": 2,
                "shift": 5,
                "w": 1,
                "x": 229,
                "y": 122
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "c4f38ccd-6fd3-4fb9-9098-3ed5b67ae599",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 64,
                "y": 122
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "83f3b42c-3be0-443a-a5fb-e7b238cacff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 8,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 179,
                "y": 122
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "0abefc12-5fd1-4809-842d-ea500ccd8d6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 116,
                "y": 122
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "6969f21d-89d9-4f59-b183-0434d1ad7e03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 13,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 212,
                "y": 102
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "1dabdb05-c188-44c4-9feb-4d5095a7a704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 14,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "f1c50bba-a9c2-49fd-a551-b88428563c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 14,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "e847e58e-3566-4476-a0b6-2b69e9f97876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 14,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "37bec607-37af-4929-8f44-d534f19c4d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 76,
                "y": 42
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "ee21ddc3-e934-4ec4-aae7-c5aeaeb94a3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "29bbd474-6683-42d2-a0b5-fc13a62a555e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "08751bc3-b2a9-44b3-9d60-cc3923846062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "1cdb651c-75f6-45e6-a075-3f37061c7360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 22
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "56b26967-1a4a-4fd2-877d-c0c0f994cb22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 41,
                "y": 22
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "dfb51910-1675-4492-b56a-dd9c88e6cec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 54,
                "y": 22
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "ce2b9f69-4f00-477e-9c86-1d674bb94cfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 14,
                "offset": -1,
                "shift": 16,
                "w": 16,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "1a4057a7-2f93-442e-9650-ac7e9d0f1a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "c81da35a-fefa-48af-ab89-be868a956449",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 62
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "f8f92891-278d-401a-a578-7f01ed28023e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 62
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "1b76cf55-0066-45ac-be25-bd2ea02aa7c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "c2d11020-b843-4973-a484-fb4e6a621803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 238,
                "y": 42
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "768ccd6a-d75f-4a1a-a7b4-0e18d054d16b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 163,
                "y": 122
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "2afeb6a3-ef14-4be0-a682-52f9cc5e826e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 159,
                "y": 122
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "9643b12b-9c04-406d-ba3f-821a7b50c943",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 14,
                "offset": -1,
                "shift": 3,
                "w": 6,
                "x": 204,
                "y": 102
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "358c496d-c919-4d55-97b3-bc5b7422f264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 106,
                "y": 122
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "bf2bc226-e6e9-4d17-9fb1-fd78867f8fa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 14,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 80,
                "y": 22
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "a4323c60-0350-408d-8eb3-59d34b6fff80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 216,
                "y": 42
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "87a2ac67-7f43-47d5-bb8e-fc13007890be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 42
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "189f0147-ad79-4d98-ba3d-273e4ef19d0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 163,
                "y": 22
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "65e0fc1d-4ed0-4933-be56-f2fca4d1a582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 235,
                "y": 22
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "12a6cd4f-1626-4e04-993e-a4e82c0c9210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 199,
                "y": 22
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "3366126e-f130-4024-8529-92bda03bb937",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 139,
                "y": 22
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "850086e2-d0c3-42ad-b217-36d3f0118f61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 12,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 47,
                "y": 102
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "b8ff500f-9e49-4332-9b33-284a40153111",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 115,
                "y": 22
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "e881742f-214c-4c5e-bc3b-27b80c83f063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 172,
                "y": 42
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "03ca1a5c-0c44-45d3-9d3d-966f01791a3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 86,
                "y": 42
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "8cb96d16-9a0f-4c8c-b4e9-755f92ccdb8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 183,
                "y": 42
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "5afea49f-8ae9-47f4-876d-ef6cfee9b60c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 161,
                "y": 42
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "13fbdde7-0469-466c-b8e9-d6a6acc88c09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 139,
                "y": 42
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "a4be105c-9f82-4c37-b6b9-50e3b0fca69f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 117,
                "y": 42
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "463045bb-6b3f-46a7-a3e2-e1a2a500f140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 210,
                "y": 82
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "6413467c-4f71-48f6-9210-cc5df0fcb7c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 228,
                "y": 82
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "7ae389fd-240a-4de7-8dbc-da828dda7a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 246,
                "y": 82
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "cd93b914-dbac-49c9-8d3d-c4e910b999e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 102
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "540e363d-e46c-4877-bd4b-951785465d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 201,
                "y": 82
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "a1a21282-97e3-4e69-98a8-87a51ba6cf69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 174,
                "y": 82
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "ac28732e-00e5-4f70-88d6-6f8403a1c83b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 102
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "0ee80f7c-3fab-439c-a4e5-dc13228cc7b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 14,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "79da087f-2c59-48e5-9999-56e31040a7bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 31,
                "y": 82
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "b53bb10a-9f30-4a1c-a84e-dad0255a5433",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 84,
                "y": 82
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "f059fabb-2a13-4093-8aab-36cccdf7bd35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 75,
                "y": 82
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "b7af6208-cea4-40d8-860a-0d0c074b2c32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 57,
                "y": 82
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "2c611804-9f27-4780-931f-43d9a2a56011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 156,
                "y": 82
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "2265f260-40d1-415f-affd-d14facabdc12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 155,
                "y": 122
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "02137e1a-07a2-41eb-bd19-b29f6be72778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 151,
                "y": 122
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "ff387e22-e572-4aa9-a9c3-d73a7a4ba7bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 14,
                "offset": -1,
                "shift": 3,
                "w": 6,
                "x": 138,
                "y": 102
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "62c0bbd7-d93f-4cba-99b0-554e47816379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 111,
                "y": 122
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "05f56337-7a0c-48fd-9875-8c032fcd3be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 111,
                "y": 82
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "111f52b6-3972-4bb4-8c9c-02df8852bea2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 170,
                "y": 102
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "8f765e80-a163-43ed-aadc-10200c9ffb7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 159,
                "y": 62
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "8e317eac-b584-4689-9c05-ea2d72c0a0e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 229,
                "y": 62
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "dc71851c-7cdf-43e4-803f-d47e1c38b41f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 219,
                "y": 62
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "d5e7ee8c-3464-4d72-b55c-e31fb33be355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 209,
                "y": 62
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "b8796922-698b-43dc-84d6-e8a30ac84777",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 82
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "38322ecc-d3f8-4132-83c2-b4de8b59dc89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 11,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 228,
                "y": 102
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "74d4e0a8-f7e5-4059-822d-8d1e33481262",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "fe525cfc-0f8d-48c7-87f0-9f9f8fbe5885",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 105,
                "y": 102
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "9c95a992-84b0-4837-9f4d-9f12ab1d6dcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 97,
                "y": 102
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "74f72cc2-6aa1-47ba-9a12-60b77c821e45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 89,
                "y": 102
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "05ad5f2e-ac74-4117-b7c7-05641faf76cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 81,
                "y": 102
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "397b3b08-eb44-46fb-ac88-20d3b939c644",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 17,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 36,
                "y": 42
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "0135f0e1-d839-47f9-b05e-206babb42b9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 123,
                "y": 62
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "e9483001-f78f-4fcf-81cf-03b7de0191c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 17,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 46,
                "y": 42
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "b6261c65-e4b4-4a82-ba29-1759f8f3e0e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "9fc98d90-fa66-4705-ad0d-b90f3317b659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "dccc0fdd-294c-4f72-b67e-c740700e6891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "35c15778-c943-412e-bef3-acf7aacb4626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "943eed1c-0792-4f12-8878-70fd4f880030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "1d9a92fe-2a42-451c-8374-d628bfde985f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "1f38d8f1-024a-4221-a1a3-dd6ce20e90b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "22871f7d-a2c6-4da1-bb7a-1865e1054278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "c81968f2-5ba6-4781-a7a4-9cde7a44e71e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "371235d5-6ea3-47e5-9b6d-8fedc9d91f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "ab52f25b-8c7a-4aeb-8f7b-543254213432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "cfdf1228-3990-402f-ab2f-88971ec578fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "d177200a-c97b-4281-9a20-882ab36b782b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "5dec2d0b-33e6-4110-8cdd-7074dfbf1fc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "4c39b1fd-fb07-49fc-befa-e5d73830713b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "329c5fcf-c9f5-4465-aed3-754e1d7784f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "f3f77ae9-d88c-4853-bc8b-8ab7f4344266",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "97639c1b-6236-40d0-b8c4-916e900164ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "fb1ec136-93f0-4331-8839-7126b5a53a37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "c330cdf0-4fab-4240-b7bb-2c84cc25206d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "7a2ea415-7462-4170-9bec-e4f54b356c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "91dde0ba-5393-425b-abf1-73739402b68e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "1fedbb31-3c45-4ee8-8b73-3f1d59f2ae0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "a541a6e9-4f9f-4c8c-9475-7e87345789c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "99f0d0c7-0671-4162-8bd8-1d1f1147b765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "2077b33b-1f16-4f2a-82dd-c3280ab8849d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "95d38413-5c52-494a-8028-ef5ad3d1b887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "a90dee33-b92c-4376-939d-e2357827b646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "abe714cc-6f65-4f98-a8fe-4daf9844e407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "f679f22a-cc9e-40e6-a7e4-b7fded5af821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "39d197e5-97ac-49be-9956-396d6ee239f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "ef09fd87-b7c8-4fe7-ac49-e9cb05e8eed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "54effa68-581a-4d36-b245-e9885d07391d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "4a2d4c28-e825-48c4-999d-41b92adfc290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "2e852354-194e-4e72-9d05-fd4b02f83a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "eae80960-3be2-484b-826b-d9155620143d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "d9e9ada4-1ea0-463e-911c-7968296a2eb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "0b169f79-5755-4aca-a1b4-22d1f95d5bac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "6d2b892b-b521-4c3d-8c6b-f507860e1b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "9f38f6dc-45c7-493c-9302-508ef968255a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "b0918c0b-760f-4168-81d6-cc1106355472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "210b8bd0-7670-4277-bb2e-2e45c36dd154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "949a332a-93fd-4325-a296-dfcbf6abafcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "958dcdf2-7d44-4e72-a63f-b6407627f33b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "876278e5-6314-4101-a442-421ecbd1b057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "4cf4347e-63a4-40b6-a899-ab05746cfe9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "6e287f03-109b-48a3-ae51-5aa2b1701380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "d2c0bae4-582c-42c8-9433-c7da67ba12cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "42cd302b-ba8d-499c-95c8-d2d675ce78dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "4045a718-ef4f-49d5-92f5-8eba41e1bf5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "1e62d1db-dc52-436c-b991-59c96213e576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "7e3ce6be-1199-4f47-984c-c839481bab65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "da584ee4-8545-466b-8a80-dc71c4bc71cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "38fdbe82-7eec-4e9c-9819-e60492bb96d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "0980304f-4263-4377-9db1-f058491c73ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "ead25b34-2cf8-4e9a-90c4-7205ab316d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "1da0635e-21f0-4981-90aa-76fd11b9a8ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "450894c2-9f83-4933-afb4-1a0ac7f8e6ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "f5634a2f-12ce-4ad8-a651-f589cc06c3e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "08489047-ee1b-4f05-8fc6-85db0c9d561e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "2ed94c7c-65fa-49ab-bb35-e51fd5231935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "7187ce57-55a2-4b7e-ac9b-769ea4be825e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "b6f53bd9-e6ca-433f-9b50-b136148c8704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "c0e1a8d7-a9ba-4d12-bf60-ba66846f12e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "f7be41f5-785b-4ec1-9734-58a23bf4fdda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "b5425d1e-c562-438b-8b38-33cd336b2d90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "b1c46421-fd4a-4758-936a-daf8d8699c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "777497ed-557f-4d00-929d-58909cbc2835",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "209444a2-c77b-4d92-9186-20ecf633a2af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "9f6b90bf-fa54-4f3c-b616-84c4a2039448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "bc6dc3ad-698e-4bc6-a23d-dca04acf0d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "d4b364c6-fe1c-4f1e-9394-ab9b44bb28e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 65
        },
        {
            "id": "d9340e22-cc83-4454-a1e0-30eff1376b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 902
        },
        {
            "id": "87e74580-e25d-4adb-8772-bc60c701e4ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 913
        },
        {
            "id": "0cf04ee2-fb68-4132-99d0-e86b2d125994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 916
        },
        {
            "id": "624ee677-5fd4-4bbd-ac83-4c27ca4a5dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 923
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}