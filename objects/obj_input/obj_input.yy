{
    "id": "0d533319-8830-4ac3-bf55-806d9714134a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_input",
    "eventList": [
        {
            "id": "014349b6-ce25-47b2-8062-7d60a5d13092",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d533319-8830-4ac3-bf55-806d9714134a"
        },
        {
            "id": "dad16c3c-ffda-44e4-a4c6-463322089566",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0d533319-8830-4ac3-bf55-806d9714134a"
        },
        {
            "id": "9d76bb22-36f4-4ec1-bdac-34e1536db4c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0d533319-8830-4ac3-bf55-806d9714134a"
        },
        {
            "id": "37d3f0d3-ed39-4451-b111-72228970e4fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "0d533319-8830-4ac3-bf55-806d9714134a"
        },
        {
            "id": "3dadf4f5-a2bb-469a-bed9-3d55715d48f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 35,
            "eventtype": 9,
            "m_owner": "0d533319-8830-4ac3-bf55-806d9714134a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}