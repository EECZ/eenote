{
    "id": "b9966148-0121-4ba1-9c59-ebda7f04820b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_help_text",
    "eventList": [
        {
            "id": "c76a49e7-53ab-4340-a16b-896f757ea7a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b9966148-0121-4ba1-9c59-ebda7f04820b"
        },
        {
            "id": "ed4dde5f-720d-430d-8b52-6ba1357b483f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 36,
            "eventtype": 9,
            "m_owner": "b9966148-0121-4ba1-9c59-ebda7f04820b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}