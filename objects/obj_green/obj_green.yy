{
    "id": "46b1bec7-7e69-4c26-b9d6-a1d079c9c947",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_green",
    "eventList": [
        {
            "id": "f2002243-ad3a-47cd-8496-589386323244",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "46b1bec7-7e69-4c26-b9d6-a1d079c9c947"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "ed169aad-f775-48c2-8e9c-a04d912a002e",
    "visible": true
}