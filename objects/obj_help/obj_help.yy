{
    "id": "1fb6ab14-5483-498b-bf20-72003ff9f5a2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_help",
    "eventList": [
        {
            "id": "fe30b529-13e2-4ee9-b5a3-45bb2090ae75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "1fb6ab14-5483-498b-bf20-72003ff9f5a2"
        },
        {
            "id": "11c5dfc2-6df9-4472-9141-1f3a463146f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "1fb6ab14-5483-498b-bf20-72003ff9f5a2"
        },
        {
            "id": "fa7a86f1-cc4c-4726-ac13-70a0fc9bfdcf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "1fb6ab14-5483-498b-bf20-72003ff9f5a2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "f2c0a92b-4972-4fc4-aaf2-2498cc798612",
    "visible": true
}