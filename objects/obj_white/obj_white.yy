{
    "id": "43616f4d-07ab-449e-88f3-616cf8eaa865",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_white",
    "eventList": [
        {
            "id": "6bd9685b-115f-4350-a83f-06c6bbf95848",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "43616f4d-07ab-449e-88f3-616cf8eaa865"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "c84a0133-f7b5-4879-9e40-aa74e2386053",
    "visible": true
}