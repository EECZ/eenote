{
    "id": "d64b0c82-4f80-4509-9b65-b1ba67434ebc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blue",
    "eventList": [
        {
            "id": "d68e37a6-739b-4d6f-922f-af13af024f32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d64b0c82-4f80-4509-9b65-b1ba67434ebc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "14f1b54b-da8c-40db-9e43-9f10efa4aa97",
    "visible": true
}