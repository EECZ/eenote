{
    "id": "3fecfff6-824e-41b4-8a77-cb66acd4e544",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_red",
    "eventList": [
        {
            "id": "2e9c5e3a-8417-4170-8f1d-3c6ac1983fb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "3fecfff6-824e-41b4-8a77-cb66acd4e544"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "615c01e2-6131-424f-97bd-0ca05d575fb1",
    "visible": true
}